# metabase-db2-driver
v7r1 iSeries DB2 Driver for Metabase

### Prereqs: Install Metabase locally, compiled for building drivers 

```bash
cd /path/to/metabase/source
wget desired version of metabase source from: https://github.com/metabase/metabase/releases/xxxx...
unzip / untar
lein install-for-building-drivers
```

### Build the DB2 driver using the newly-built metabase as a dependency
#### 1.) Navigate to the folder which contains the "Dockerfile" file
#### 2.) Edit the Dockerfile and change the variable "METABASE_VERSION" to your desired version of metabase
##### - You can get the db2.clj from here https://github.com/damienchambe/metabase-db2-driver/blob/master/src/metabase/driver/db2.clj
##### Note: We commented out the "defmethod driver/humanize-connection-error-message" because its not working anymore. Probably deprecated
#### 3.) Build the image.
```bash
sudo DOCKER_BUILDKIT=1 docker build ./
```
#### 4.) Next, get the built driver inside the image.
##### - First, run the command "sudo docker images" to get the image id at you built
``` bash
sudo docker images
```
##### - Next, run the following commands
```bash
id=$(docker create image-name_image-id)
sudo docker cp $id:pathFromImage FolderToPaste
sudo docker rm -v $id
```

#### 5.) Open the "FolderToPaste" folder you downloaded. We only need the db2.metabasedriver and the jt400.jar. Move it to the folder you will mount in #7. 
```bash
cp ./FolderToPaste/target/db2.metabase-driver.jar ~/metabase_includes/plugins_notime42/
```

#### 6.) Check the permissions on the said folder where you put the driver
##### - Run the this command to check 
```bash
ls -l
```
##### - if not complete run the command
```bash
chmod 777
```
#### 7.) Run metabase on docker 
##### - make sure to change the port, the databasename, metabase version tag, 
##### - the pluginsfolder name after --mount should be the folder you used above, where you moved the db2 drier and the jt400.jar)
```bash
sudo docker run -d -p 3001:3000 \
            -e "MB_DB_TYPE=mysql" \
            -e "MB_DB_DBNAME=metabase" \
            -e "MB_DB_PORT=3306" \
            -e "MB_DB_USER=<user>" \
            -e "MB_DB_PASS=<password>l" \
            -e "MB_DB_HOST=<host_ip>" \
            -e "JAVA_OPTS=-Xmx2g" \ 
            -e "TZ=Asia/Manila" \
            --mount type=bind,source=<source_full_path>,destination=/plugins \
            --name metabase metabase/metabase
```
